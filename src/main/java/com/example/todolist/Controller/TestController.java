package com.example.todolist.Controller;

import com.example.todolist.Entity.DoUuTien;
import com.example.todolist.Service.DoUuTienService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
@RequestMapping("/douutien")
public class TestController {
    @Autowired
    private DoUuTienService doUuTienService;
    @GetMapping("/get")
    public void getDoUuTien(){
        DoUuTien uuTien =doUuTienService.getDoUuTienByName("Low");
        System.out.println(uuTien.getTenDoUuTien());
    }
}
