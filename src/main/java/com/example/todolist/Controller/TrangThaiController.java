package com.example.todolist.Controller;

import com.example.todolist.Service.TrangThaiService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/trangthai")
public class TrangThaiController {
    @Autowired
    private TrangThaiService trangThaiService;
    @GetMapping("/get")
    public ResponseEntity<?> getTrangThai(){
        System.out.println(trangThaiService.getTrangThai());
        return new ResponseEntity<>(trangThaiService.getTrangThai(), HttpStatus.OK);
    }

}
