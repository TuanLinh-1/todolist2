package com.example.todolist.Entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter @Setter
@Entity
@Table(name = "CongViec")
public class CongViec {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private String maCongViec;
    private String tenCongViec;
    private int tienDo;

    @OneToMany(mappedBy = "nhiemVuCongViec")
    @JsonIgnore
    private List<NhiemVu> nhiemVuList;


    @ManyToOne
    @JoinColumn(name = "idTrangThai")
    private TrangThai TrangThaiCongViec;

    @ManyToOne
    @JoinColumn(name = "idDoUuTien")
    private DoUuTien DoUuTienCongViec;


}
