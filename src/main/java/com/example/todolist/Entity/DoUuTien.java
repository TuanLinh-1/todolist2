package com.example.todolist.Entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Entity
@Table(name = "DoUuTien")
public class DoUuTien {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private String tenDoUuTien;

    @OneToMany(mappedBy = "DoUuTienCongViec")
    @JsonIgnore
    private List<CongViec> congViecList;

    @OneToMany(mappedBy = "DoUuTienNhiemVu")
    @JsonIgnore
    private List<NhiemVu> nhiemVuList;

}
