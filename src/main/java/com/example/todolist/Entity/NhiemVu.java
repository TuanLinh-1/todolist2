package com.example.todolist.Entity;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Entity
@Table(name = "NhiemVu")
public class NhiemVu {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private String maNhiemVu;
    private String tenNhiemVu;

    @ManyToOne
    @JoinColumn(name = "idCongViec")
    private CongViec nhiemVuCongViec;

    @ManyToOne
    @JoinColumn(name = "idTrangThai")
    private TrangThai TrangThaiNhiemVu;

    @ManyToOne
    @JoinColumn(name = "idDoUuTien")
    private DoUuTien DoUuTienNhiemVu;
}
