package com.example.todolist.Entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.*;

import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Getter
@Setter
@Entity
@Table(name = "TrangThai")
public class TrangThai {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private String maTrangThai;
    private String tenTrangThai;

//    private  long idTienTrinh;

    @ManyToOne
    @JoinColumn(name = "idTienTrinh")
    private TienTrinh idTienTrinh;


    @OneToMany(mappedBy = "TrangThaiCongViec",fetch = FetchType.LAZY)
    @JsonIgnore
    private List<CongViec> congViecList;

    @OneToMany(mappedBy = "TrangThaiNhiemVu",fetch = FetchType.LAZY)
    @JsonIgnore
    private  List<NhiemVu> nhiemVuList;


}
