package com.example.todolist.EntityRequest;

import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Getter
@Setter
public class TrangThaiRequest {
    private long id;
    private int maTrangThai;
    private String tenTrangThai;
}
