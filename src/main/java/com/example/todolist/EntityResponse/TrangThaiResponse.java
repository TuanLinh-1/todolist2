package com.example.todolist.EntityResponse;

import com.example.todolist.Entity.TienTrinh;
import com.example.todolist.Entity.TrangThai;
import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Getter
@Setter
public class TrangThaiResponse {
    private long id;
    private String maTrangThai;
    private String tenTrangThai;
    private TienTrinh trangThaiTienTrinh;
}
