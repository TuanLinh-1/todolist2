package com.example.todolist.Mapping;

import com.example.todolist.Entity.TienTrinh;
import com.example.todolist.Entity.TrangThai;
import com.example.todolist.EntityResponse.TrangThaiResponse;

public class TrangThaiMapping {

    public static TrangThaiResponse mapTrangThaiToTrangThaiRespons(TrangThai trangThai){
        TrangThaiResponse trangThaiResponse = new TrangThaiResponse();

        trangThaiResponse.setId(trangThai.getId());
        trangThaiResponse.setMaTrangThai(trangThai.getMaTrangThai());
        trangThaiResponse.setTenTrangThai(trangThai.getTenTrangThai());
        trangThaiResponse.setTrangThaiTienTrinh(trangThai.getIdTienTrinh());
        System.out.println(trangThaiResponse.getTrangThaiTienTrinh());
        return trangThaiResponse;
    }

}
