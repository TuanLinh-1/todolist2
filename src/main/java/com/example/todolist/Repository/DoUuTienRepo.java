package com.example.todolist.Repository;

import com.example.todolist.Entity.DoUuTien;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;


public interface DoUuTienRepo extends JpaRepository<DoUuTien,Long> {
    @Query("select u from DoUuTien u where u.tenDoUuTien = ?1")
    Optional<DoUuTien> getDoUuTienByName (String tenDoUuTien);
}
