package com.example.todolist.Repository;

import com.example.todolist.Entity.TrangThai;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TrangThaiRepo extends JpaRepository<TrangThai,Long> {
}
