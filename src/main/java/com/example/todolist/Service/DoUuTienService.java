package com.example.todolist.Service;

import com.example.todolist.Entity.DoUuTien;

import java.util.List;

public interface DoUuTienService {

    List<DoUuTien> getDoUuTienById();
    DoUuTien getDoUuTienByName(String name);
}
