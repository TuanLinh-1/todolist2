package com.example.todolist.Service.Impl;

import com.example.todolist.Entity.DoUuTien;
import com.example.todolist.Repository.DoUuTienRepo;
import com.example.todolist.Service.DoUuTienService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class DoUuTienServiceImpl implements DoUuTienService {
    @Autowired
    private DoUuTienRepo doUuTienRepo;

    @Override
    public DoUuTien getDoUuTienByName(String name){
        Optional<DoUuTien> uuTienOptional = doUuTienRepo.getDoUuTienByName(name);
        if(uuTienOptional.isPresent()){
            DoUuTien doUuTien = uuTienOptional.get();
            return doUuTien;
        }
        return null;
    }
    @Override
    public List<DoUuTien> getDoUuTienById(){
        List<DoUuTien> doUuTiens = doUuTienRepo.findAll();
        return doUuTiens;
    }


}
