package com.example.todolist.Service.Impl;

import com.example.todolist.Entity.TrangThai;
import com.example.todolist.EntityResponse.TrangThaiResponse;
import com.example.todolist.Mapping.TrangThaiMapping;
import com.example.todolist.Repository.TrangThaiRepo;
import com.example.todolist.Service.TrangThaiService;
import com.example.todolist.api.ResponseAPI;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class TrangThaiServiceImpl implements TrangThaiService {
    @Autowired
    private TrangThaiRepo trangThaiRepo;

    @Override
    public ResponseAPI getTrangThai(){
        try{
            List<TrangThai> trangThaiList = trangThaiRepo.findAll();
            List<TrangThaiResponse> trangThaiResponse = trangThaiList.stream().map(TrangThaiMapping::mapTrangThaiToTrangThaiRespons).collect(Collectors.toList());
            System.out.println(trangThaiResponse);
            return new ResponseAPI(true,"Lấy dữ liệu thành công",trangThaiResponse);
        }catch (Exception e){
            return new ResponseAPI(false,e.getMessage(),null);
        }
    }
}
