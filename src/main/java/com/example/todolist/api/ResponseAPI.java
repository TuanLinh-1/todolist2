package com.example.todolist.api;

import lombok.*;

import java.util.List;
@NoArgsConstructor
@AllArgsConstructor
@Data
@Getter
@Setter
public class ResponseAPI {
    private boolean status;
    private String message;
    private List content;
}
